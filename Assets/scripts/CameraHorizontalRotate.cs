﻿using UnityEngine;
using System.Collections;

public class CameraHorizontalRotate : MonoBehaviour {

    public float speed = 1.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            speed /= 1.1f;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            speed *= 1.1f;
        }
        /*if ( Input.GetMouseButtonDown(0) )
        {*/
        float x = Input.GetAxis("Mouse X") * speed * Time.deltaTime;
        Transform t = GameObject.FindWithTag("Player").transform;//GameObject.FindWithTag("Player").transform;
        //Transform tC = GameObject.FindWithTag("MainCamera").transform;
        GameObject.FindWithTag("MainCamera").transform.RotateAround(t.position, t.up, x);
        GameObject.FindWithTag("MainCamera").transform.LookAt(t);
    }
}
