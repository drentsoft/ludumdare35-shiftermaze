﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    public float sprint = 10.0f;
    public float speed = 2.0f;
    public float acc = 0.5f;
    float finalSpeed;
    // Use this for initialization
    void Start () {
        finalSpeed = speed;
	}
	
	// Update is called once per frame
	void Update () {
        float f = Input.GetAxis("Vertical");
        float s = Input.GetAxis("Horizontal");
        Vector3 forward = GameObject.FindWithTag("MainCamera").transform.TransformDirection(Vector3.forward);
        forward.y = 0;
        forward = forward.normalized;
        Vector3 right = new Vector3(forward.z, 0, -forward.x);
        Vector3 moveDirection = (s * right + f * forward).normalized;
        Rigidbody rb = GameObject.FindWithTag("Player").GetComponent<Rigidbody>();
        if (Input.GetButtonDown("Jump"))
            LevelGenerator.moveToSpawn();
        if( Input.GetButtonDown("Fire3"))
            finalSpeed = sprint;
        if (Input.GetButtonUp("Fire3"))
            finalSpeed = speed;
        rb.velocity = moveDirection * finalSpeed;
    }
}
