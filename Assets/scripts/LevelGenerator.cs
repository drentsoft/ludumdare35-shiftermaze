﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {

    public Texture2D levelGen;
    public readonly Color WALL = Color.white;
    public readonly Color PATH = Color.black;
    public readonly Color START = new Color(1.0f, 1.0f, 0.0f); // Yellow but not the Unity default for yellow
    public readonly Color GOAL = Color.green;
    public readonly Color CUBEGATE = Color.red;// blue;
    public readonly Color CUBEPLATE = Color.magenta;//cyan;
    public readonly Color SPHEREGATE = Color.blue;//Color.red;
    public readonly Color SPHEREPLATE = Color.cyan;// magenta;
    public readonly Color TRAP = Color.grey;

    public GameObject cubeGate;
    public GameObject cubePlate;
    public GameObject sphereGate;
    public GameObject spherePlate;
    public GameObject wall;
	public GameObject floor;
	public GameObject startObj;
    public GameObject goal;

    public static int cubeSize = 2;

    static int startX;
    static int startY;

    public static string nextScene;

    // Use this for initialization
    void Start ()
    {
        SwitchTrigger.sphere = GameObject.Find("PlayerSphere");
        SwitchTrigger.cube = GameObject.Find("PlayerCube");
        if ( levelGen.GetPixel(0,0) == CUBEGATE )
        {
            SwitchTrigger.sphere.SetActive(false);
            SwitchTrigger.cube.SetActive(true);
        } else
        {
            SwitchTrigger.cube.SetActive(false);
            SwitchTrigger.sphere.SetActive(true);
        }
		for( int y = 0; y < levelGen.height; y++ )
        {
			for (int x = 0; x < levelGen.width; x++ )
            {
                Color pixCol = levelGen.GetPixel(x, y);
                    if (x == 0 && y == 0)
                        continue;
                if (pixCol == PATH)
                {
                    spawnFloor(x, y);
                }
                else if ( pixCol == WALL )
                {
                    spawnWall(x, y);
                }
                else if( pixCol == START )
                {
                    spawnStart(x, y);
                }
                else if (pixCol == GOAL)
                {
                    spawnGoal(x, y);
                }
                else if (pixCol == CUBEGATE)
                {
                    spawnGate(x, y, cubeGate);
                }
                else if (pixCol == SPHEREGATE)
                {
                    spawnGate(x, y, sphereGate);
                }
                else if (pixCol == CUBEPLATE )
                {
                    // Create a cube trigger plate
                    spawnPlate(x, y, cubePlate);
                }
                else if (pixCol == SPHEREPLATE)
                {
                    // Create the sphere plate
                    spawnPlate(x, y, spherePlate);
                }
                else if (pixCol == TRAP)
                {
                    // Create no path block
                }
            }
        }
        GameObject.Find("DeathPlane").transform.localScale = new Vector3(levelGen.width, 1, levelGen.height);
    }

    void spawnFloor( int x, int y )
    {
        // Spawn a floor block
        GameObject nFloor = GameObject.Instantiate(floor);
        nFloor.transform.Translate(new Vector3(x * cubeSize, 0, y * cubeSize));
    }

    void spawnGate( int x, int y, GameObject gate )
    {
        int valid = checkValidGateLocation(x, y);
        GameObject nGate;
        if (valid == 1 || valid == 2)
        {
            nGate = Instantiate(gate);
            nGate.transform.Translate(new Vector3(x * cubeSize, cubeSize - 1.05f, y * cubeSize));
            if (valid == 1)
            {
                nGate.transform.Rotate(Vector3.up * 90);
            }
            if (gate.Equals(sphereGate))
                Debug.Log("Sphere gate created.");
            else if (gate.Equals(cubeGate))
                Debug.Log("Cube gate created.");
        }
        else
        {
            spawnFloor(x, y);
        }
    }

    void spawnWall( int x, int y )
    {
        // Spawn a wall block
        GameObject nWall = GameObject.Instantiate(wall);
        nWall.transform.Translate(new Vector3(x * cubeSize, cubeSize, y * cubeSize));
        //Debug.Log ("Wall tile created.");
    }

    void spawnStart( int x, int y )
    {
        // Create the start block
        GameObject nStart = GameObject.Instantiate(startObj);
        nStart.transform.Translate(new Vector3(x * cubeSize, 0, y * cubeSize));
        GameObject.FindWithTag("MainCamera").transform.SetParent(GameObject.FindWithTag("Player").transform);
        startX = x * cubeSize;
        startY = y * cubeSize;
        moveToSpawn();
        Debug.Log("Start tile created.");
    }
    
    void spawnGoal( int x, int y )
    {
        // Create the goal block
        GameObject nGoal = GameObject.Instantiate(goal);
        nGoal.transform.Translate(new Vector3(x * cubeSize, 0, y * cubeSize));
        Debug.Log("Goal tile created.");
    }

    void spawnPlate( int x, int y, GameObject plate )
    {
        GameObject nPlate = GameObject.Instantiate(plate);
        nPlate.transform.Translate(new Vector3(x * cubeSize, 1, y * cubeSize));
        if( plate.Equals(cubePlate) )
            Debug.Log("Cube plate created.");
        if (plate.Equals(spherePlate))
            Debug.Log("Sphere plate created.");
    }

    public static void moveToSpawn()
    {
        GameObject.FindWithTag("Player").transform.position = new Vector3(startX, cubeSize, startY);
    }

    int checkValidGateLocation( int x, int y )
    {
        if( levelGen.GetPixel(x-1,y) == PATH && levelGen.GetPixel(x + 1, y) == PATH )
        {
            // Left to right gate
            return 1;
        } else if(levelGen.GetPixel(x, y - 1) == PATH && levelGen.GetPixel(x, y + 1) == PATH )
        {
            // Front to back gate
            return 2;
        } else
        {
            return 0;
        }
    }
}
