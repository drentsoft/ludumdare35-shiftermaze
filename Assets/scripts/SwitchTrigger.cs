﻿using UnityEngine;
using System.Collections;

public class SwitchTrigger : MonoBehaviour {

    public static GameObject cube;
    public static GameObject sphere;

    int state = 0;

    void Start()
    {

    }

    /*void Update()
    {
        if( Input.GetButtonDown("Fire1") )
        {
            switchPlayer(state);
            state += 1;
            if (state > 1)
                state = 0;
        }
    }*/

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag.Equals("Player"))
        {
            Transform t = coll.transform;
            Debug.Log("PLAYER!");
            if (coll.gameObject.name.Contains("PlayerSphere") && gameObject.tag.Equals("CubePlate"))
            {
                switchPlayer(0);
            }
            else if (coll.gameObject.name.Contains("PlayerCube") && gameObject.tag.Equals("SpherePlate"))
            {
                switchPlayer(1);
            }
            else
            {
                Debug.Log("Colliding with the same type");
            }
        }
    }

    void switchPlayer( int x)
    {
        GameObject cam = GameObject.FindWithTag("MainCamera");
        if ( x == 0 )
        {
            cube.SetActive(true);
            sphere.SetActive(false);
            cube.transform.position = sphere.transform.position;
            cube.transform.Translate(0, 0.1f, 0);
            cube.transform.rotation = sphere.transform.rotation;
            cam.transform.SetParent(cube.transform);
        } else if( x == 1 )
        {
            sphere.SetActive(true);
            cube.SetActive(false);
            sphere.transform.position = cube.transform.position;
            sphere.transform.rotation = cube.transform.rotation;
            cam.transform.SetParent(sphere.transform);
        }
    }

}
