﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GoalTrigger : MonoBehaviour {

    bool showGUI;
    NextLevel nL;

	// Use this for initialization
	void Start () {
        nL = GameObject.Find("LevelGenerator").GetComponent<NextLevel>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter( Collision coll )
    {
        //Debug.Log(coll.tag);
        if ( coll.gameObject.tag.Equals("Player"))
        {
            Debug.Log("WIN!");
            showGUI = true;
            SceneManager.LoadScene(nL.nextLevel);
        }
    }

    void OnGUI()
    {
        if( showGUI)
        {
            GUI.color = new Color(1, 1, 0.5f, 1);
            GUI.backgroundColor = new Color(0, 0, 0, 0.5f);
            GUI.Label(new Rect(50, 50, 100, 100), "YOU FOUND THE GOAL!");
        }
    }
}
